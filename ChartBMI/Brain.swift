//
//  Brain.swift
//  ChartBMI
//
//  Created by Ismail Nassurally on 09/12/2018.
//  Copyright © 2018 Ismail Nassurally. All rights reserved.
//

import Foundation

enum ErrorTags:String {
    case BMIApproved = ""
    case BMIInfinity = "The values supplied gave infinity"
    case BMIZero = "BMI cannot be zero"
    case BMIExtreme = "This BMI is mortal, please consider seeing a doctor, or checking your Values"
    
}

enum HealthStatus:String{
    case underweight
    case normal
    case overweight
    case obese
}

enum Gender{
    case Male
    case Female
}

class Month{
    var name:Int = 1
    var days:[Int:Double] = [:]
}

class Calculator{
    func getMonths()->[String]{
        return ["January","Feburary","March","April","May","June","July","August","September","October","November","December"]
    }
    
    func getGenderFromString(string:String)->Gender{
        if(string == "m" ||
            string == "M"){return .Male}
        else{return .Female}
    }
    
    func getBMIFrom(weight:Double, height:Double)->(answer:Double,errorTag:ErrorTags){
        if(height == 0){
            return (0,.BMIInfinity)
        }else if( weight == 0){
            return (0,.BMIZero)
        }
        
        let BMI:Double = weight / (height * height)
        
        if (BMI < 5.00 ||
            BMI > 500.0 ){
            return (BMI,.BMIExtreme)
        }
        
        return(BMI,.BMIApproved)
    }
    
    func getStatusFromBMIAndSex(BMI:Double,Sex:Gender)->HealthStatus{
        var _BMI = BMI
        if(Sex == .Female){
            _BMI = BMI - 1
        }
        switch _BMI {
        case 1..<18.5:
            return .underweight
        case 18.5..<24.9:
            return .normal
        case 24.9..<29.9:
            return .overweight
        case 29.9..<500:
            return .obese
        default:
            return .underweight
        }
    }
}
