//
//  ViewController.swift
//  ChartBMI
//
//  Created by Ismail Nassurally on 08/12/2018.
//  Copyright © 2018 Ismail Nassurally. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    var calculator:Calculator?
    var pickerData:[String] = [String]()
    
    var days:[Int] = []
    var BMIStats:[Double] = []
    
    var yearsData:[Int:[Month]] = [:]
    
    var finalMonthData:[Int:Double] = [:]
    var finalMonthInt:Int = 0
    var finalYearInt:Int = 0
    
    var didTouchAtLessTHanKeyboardHeight:Bool = false
    
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var sexTextField: UITextField!
    @IBOutlet weak var heightMTextField: UITextField!
    @IBOutlet weak var heightCMTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var BMITextField: UITextField!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var errorMessageTextField: UILabel!
    
    @IBOutlet weak var monthUIPicker: UIPickerView!
    @IBOutlet weak var dateUIPicker: UIDatePicker!
    
    
    @IBAction func yearTextFieldTouchedDown(_ sender: Any) {
        didTouchAtLessTHanKeyboardHeight = true
    }
    
    @IBAction func btnShowHistoryPressed(_ sender: Any) {
        yearTextField.resignFirstResponder()
        guard let yearText = yearTextField.text else{return}
        guard let yearInt = Int(yearText) else {return}
        let monthSelected = monthUIPicker.selectedRow(inComponent: 0)
        guard let yearData = yearsData[yearInt] else {return}
        var monthdata:[Int:Double] = [:]
        for month in yearData{
            if (month.name == monthSelected + 1){
                monthdata = month.days
                break
            }
        }
        finalMonthData = monthdata
        finalYearInt = yearInt
        finalMonthInt = monthSelected + 1
        self.performSegue(withIdentifier: "showGraphViewSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let chartViewController = segue.destination as? ChartViewController{
            chartViewController.passedMonthData = finalMonthData
            chartViewController.passedMonthInt = finalMonthInt
            chartViewController.passedYearInt = finalYearInt
            
        }
    }
    
    @IBAction func btnCalculateBMIPressed(_ sender: Any) {
        self.view.endEditing(true)
        guard let weight = weightTextField.text else{return}
        guard let heightM = heightMTextField.text else{return}
        guard let heightCM = heightCMTextField.text else{return}
        guard let gender = sexTextField.text else{return}
        
        guard let weightDouble = Double(weight) else{return}
        guard let heightDouble = Double(heightM+"."+heightCM) else{return}
        
        let BMI = calculator!.getBMIFrom(weight: weightDouble, height: heightDouble)
        
        errorMessageTextField.text = BMI.errorTag.rawValue
        
        guard BMI.errorTag == .BMIApproved else {return}
        
        BMITextField.text = String(BMI.answer)
        
        let status = calculator!.getStatusFromBMIAndSex(BMI: BMI.answer, Sex: calculator!.getGenderFromString(string: gender))
        
        statusTextField.text = String(status.rawValue)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormatter.string(from: dateUIPicker.date)
        let splitDate = dateString.components(separatedBy:"/")
        let _day = Int(splitDate[0])!
        let _month = Int(splitDate[1])!
        let _year = Int(splitDate[2])!
        
        let newMonth = Month()
        newMonth.name = _month
        newMonth.days = [_day:BMI.answer]
        if let selectedYear = yearsData[_year] {
            var containsSelectedMonth = false
            for (index,eachMonth) in selectedYear.enumerated()  {
                if (eachMonth.name == _month){
                    yearsData[_year]![index].days[_day] = BMI.answer
                    containsSelectedMonth = true
                }
            }
            if(!containsSelectedMonth){
                yearsData[_year]!.append(newMonth)
            }
            
        } else {
            yearsData[_year] = [newMonth]
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //UI Textfield Delegates to restrict user input:
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharactersSexTextField = CharacterSet(charactersIn: "mfMF")
        let allowedCharactersIntegerTextField = CharacterSet(charactersIn: "0123456789")

        if textField == sexTextField {
            return didStringConformtoTextfield(string: string, textField, characterSet: allowedCharactersSexTextField, maxLength: 1, range: range)
        }
        else if textField == weightTextField {
            return didStringConformtoTextfield(string: string, textField, characterSet: allowedCharactersIntegerTextField, maxLength: 4, range: range)
        }
        else if textField == heightMTextField ||
            textField == heightCMTextField {
            return didStringConformtoTextfield(string: string, textField, characterSet: allowedCharactersIntegerTextField, maxLength: 2, range: range)
        }
        else if textField == yearTextField {
            return didStringConformtoTextfield(string: string, textField, characterSet: allowedCharactersIntegerTextField, maxLength: 4, range: range)
        }
        return false
    }
    
    private func didStringConformtoTextfield(string:String, _ textField:UITextField,characterSet:CharacterSet, maxLength:Int,range:NSRange)->Bool{
        let inputCharacterSet = CharacterSet(charactersIn: string)
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= maxLength && characterSet.isSuperset(of: inputCharacterSet)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        calculator = Calculator()
        pickerData = calculator!.getMonths()
    
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        weightTextField.delegate = self
        sexTextField.delegate = self
        heightMTextField.delegate = self
        heightCMTextField.delegate = self
        yearTextField.delegate = self
        
        monthUIPicker.delegate = self
        monthUIPicker.dataSource = self

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) ->Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component(column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }

    
    deinit{
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    
    @objc func keyboardWillChange(notification:Notification){
        
        guard didTouchAtLessTHanKeyboardHeight else {
            return
        }
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else{
            return
        }
        if notification.name == UIResponder.keyboardWillShowNotification ||
            notification.name == UIResponder.keyboardWillChangeFrameNotification{
            
            view.frame.origin.y = -keyboardRect.height
        
        }else{
            view.frame.origin.y = 0
            didTouchAtLessTHanKeyboardHeight = false
        }
    }


}

