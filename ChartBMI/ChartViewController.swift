//
//  ChartViewController.swift
//  ChartBMI
//
//  Created by Ismail Nassurally on 10/12/2018.
//  Copyright © 2018 Ismail Nassurally. All rights reserved.
//

import UIKit
import Charts

class ChartViewController: UIViewController {
    
    @IBOutlet weak var barChartView: BarChartView!
    var passedMonthData:[Int:Double] = [:]
    var passedMonthInt:Int = 0
    var passedYearInt:Int = 0
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.dismiss(animated:true, completion: nil
        )
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monthLabel.text = "\(monthLabel.text!) \(passedMonthInt)"
        yearLabel.text = "\(yearLabel.text!) \(passedYearInt)"
        // Do any additional setup after loading the view.
        
        let sortedDays = Array(passedMonthData.keys).sorted()
        var sortedBMIs:[Double] = []
        
        for day in sortedDays {
            sortedBMIs.append(passedMonthData[day]!)
        }
        
        print("the passed days is \(sortedDays)")
        print("the passed values is \(sortedBMIs))")
        
        setChart(sortedDays.map{String($0)}, values: sortedBMIs)
        
    }
    
    func setChart(_ dataPoints: [String], values: [Double]){
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "BMI")
        let chartData = BarChartData(dataSet: chartDataSet)
        barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: dataPoints)
        barChartView.data = chartData
        barChartView.xAxis.granularityEnabled = true
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.labelPosition = .bottom
        barChartView.xAxis.labelCount = 30
        barChartView.xAxis.granularity = 2
        barChartView.noDataText = "No data for this month"
    }
}
